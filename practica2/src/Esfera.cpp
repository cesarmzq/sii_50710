// César Mazquiaran Andrade 50710// 
//Esfera.cpp: implementation of the Esfera class.
//
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	radio=0.5f;
	radionormal=0.5f;
	radiomin=0.1f;
	velocidad.x=3;
	velocidad.y=3;
	reduccion=-(0.005f);
	//rebotes=0;
	
}

Esfera::~Esfera()
{

}



void Esfera::Dibuja()
{
	glColor3ub(255,255,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{
	centro=centro+velocidad*t;
	//variacion tamaño esfera
	if (radio>radiomin)
		radio+=reduccion;
	else radio=radionormal;
		
}

void Esfera::Rapido()
{
	velocidad=velocidad*1.25;
}
