// César Mazquiaran Andrade 50710
// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "Raqueta.h"
//#include "DatosMemCompartida.h" //Incluye la definicion de Raqueta.h

class CMundo  
{
public:
	void Init();
	CMundo();
	virtual ~CMundo();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();
/*P4*/	void RecibeComandosJugador();

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
//P4	Se encarga el clienbte de proyectar en memoria
	//DatosMemCompartida MemComp;
	//DatosMemCompartida* pMemComp;

	int fd;
	int fd1;
	int fd_t;//teclas

	int rebotes;

	int puntos1;
	int puntos2;

	//Añadir un identificador de thread como atributo de la clase MundoServidor (recuerda añadir la librería correspondiente en el fichero CMakeLists.txt).
	pthread_t hilo1;
	
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
