#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>

#include "DatosMemCompartida.h"
#include "Esfera.h"

int main()
{
	int file;
	//Declarar una variable de tipo puntero a DatosMemCompartida.
	DatosMemCompartida* pMemComp;
	
	//Abrir el fichero proyectado en memoria creado anteriormente en el tenis (open) y proyectarlo en memoria (mmap).
	char* org;
	file=open("/tmp/datosComp.txt",O_RDWR);
	org=(char*)mmap(NULL,sizeof(*(pMemComp)),PROT_WRITE|PROT_READ,MAP_SHARED,file,0);
	
	//Cerrar el descriptor de fichero.
	close(file);

	//Asignar la dirección de comienzo de la región creada al atributo de tipo puntero creado en el paso 2.
	pMemComp=(DatosMemCompartida*)org;

	
//El bot entra en un bucle infinito incluyendo una llamada a usleep(25000) para que se suspenda durante 25 ms y en cada iteración hay que añadir la lógica necesaria para que el bot, en función de las coordenadas Y de la Esfera y la Raqueta, establezca la acción a realizar.
	while(1)
	{
		float posicion_raqueta;

		posicion_raqueta=(pMemComp->raqueta1.y2+pMemComp->raqueta1.y1)/2;
		if(posicion_raqueta<pMemComp->esfera.centro.y)		
			pMemComp->accion=1;				//La raqueta sube
		else if(posicion_raqueta>pMemComp->esfera.centro.y)
			pMemComp->accion=-1;				//La raqueta baja
		else
			pMemComp->accion=0;				//La raqueta esta a la altura de la bola
		usleep(25000);
	}

	munmap(org,sizeof(*(pMemComp)));
	
	return 0;
}
