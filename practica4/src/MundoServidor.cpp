// César Mazquiaran Andrade 50710
// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <error.h>
#include <iostream>


char* org;
#define TAM_BUFF 60
#define TAM_CAD 200
#define TAM 200

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
	rebotes=0;
}

CMundo::~CMundo()
{
	close(fd_t);
	//unlink("/tmp/TuberiaTeclas");
	close(fd);
	//Cerrar la tubería adecuadamente (close en el destructor de MundoServidor).
	close(fd1);
	//unlink("/tmp/TuberiaClienteServidor");

	//munmap(org,sizeof(MemComp));
}

void* hilo_comandos(void* d);

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	char buff[TAM_BUFF];
	char buffer_cad[TAM_CAD];
	char cad[TAM];
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	if (jugador1.Rebota(esfera))
	{
		rebotes++;
	}
	if (jugador2.Rebota(esfera))
	{
		rebotes++;
	}
	
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		sprintf(cad, "Jugador 2 marco 1 punto, lleva %d", puntos2);
		write(fd, cad, sizeof(cad));
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		sprintf(cad, "Jugador 1 marco 1 punto, lleva %d", puntos1);
		write(fd, cad, sizeof(cad));
	}
	//Codigo Práctica 2
	if(puntos1==4 || puntos2==4)
		exit(0);
	
	
	if (rebotes==5)
	{
		esfera.Rapido();
		rebotes=0;
	}

//P3
	//envio ordenes 
	/*switch (pMemComp->accion)
{
	case 1: OnKeyboardDown('w',0,0);
		break;
	case 0:break;
	case -1: OnKeyboardDown('s',0,0);
		break;
}*/
	//pMemComp->esfera=esfera;
	//pMemComp->raqueta1=jugador1;
	//envio datos
	//char buffer_cad[TAM_CAD];
	//read(fd,buffer_cad,sizeof(buffer_cad));
	//sscanf(buffer_cad,"%f %f %f %f %f %f %f %f %f %f %d %d",&esfera.centro.x, &esfera.centro.y, &jugador1.x1, &jugador1.x2, &jugador1.y1, &jugador1.y2, &jugador2.x1, &jugador2.x2, &jugador2.y1, &jugador2.y2, &puntos1, &puntos2);

//P4
	//Construir la cadena de texto con los datos a enviar (sprintf()). Se debe hacer al final del OnTimer().
	
	sprintf(buffer_cad,"%f %f %f %f %f %f %f %f %f %f %d %d", esfera.centro.x,esfera.centro.y, jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, puntos1, puntos2);
	//Enviar los datos por la tubería (write).
	write(fd1,buffer_cad,sizeof(buffer_cad));
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{

	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
//	case 's':jugador1.velocidad.y=-4;break;
//	case 'w':jugador1.velocidad.y=4;break;
//	case 'l':jugador2.velocidad.y=-4;break;
//	case 'o':jugador2.velocidad.y=4;break;

	}
}

void CMundo::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

//P3
	//Crear un fichero del tamaño del atributo DatosMemCompartida (open y write) 
	//int file=open("tmp/datosComp.txt",O_RDWR|O_CREAT|O_TRUNC, 0666);	//lectura_y_escritura|creatuberia//flag->lecturayescritura
	//write (file, &MemComp, sizeof(MemComp));;

	//Proyectar el fichero en memoria (mmap). 	
	//org=(char*)mmap(NULL, sizeof (MemComp),PROT_WRITE|PROT_READ,MAP_SHARED,file,0);
	
	//Cerrar el descriptor de fichero. 
	//close (file);

	//Asignar la dirección de comienzo de la región creada al atributo de tipo puntero creado en el paso 2.
	//pMemComp=(DatosMemCompartida*) org;
	//pMemComp->accion=0;

	///Tuberia con informacion de la teclas
	mkfifo("/tmp/TuberiaTeclas",0777);
	fd_t=open("/tmp/TuberiaTeclas",O_WRONLY);
	fd=open("/tmp/TuberiaLogger",O_WRONLY);

//P4

	//El servidor abre la tubería en modo escritura en el método Init (mediante open).
	fd1=open("/tmp/TuberiaClienteServidor",O_WRONLY);

	//En el Init() del servidor, crear el thread indicándole la función a ejecutar:
	pthread_create(&hilo1, NULL, hilo_comandos, this);

}	

void* hilo_comandos(void* d)
{
      CMundo* p=(CMundo*) d;
      p->RecibeComandosJugador();
}

void CMundo::RecibeComandosJugador()
{
     while (1) {
            usleep(10);
            char cad[100];
            read(fd_t, cad, sizeof(cad));
            unsigned char key;
            sscanf(cad,"%c",&key);
            if(key=='s')jugador1.velocidad.y=-4;
            if(key=='w')jugador1.velocidad.y=4;
            if(key=='l')jugador2.velocidad.y=-4;
            if(key=='o')jugador2.velocidad.y=4;
      }
}
